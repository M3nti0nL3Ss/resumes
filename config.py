import os
class Config:
    BASE_FOLDER = os.path.dirname(os.path.abspath(__file__))
    OUTPUT_FOLDER = "{}/build".format(BASE_FOLDER)
    TEMPLATES_FOLDER = "{}/templates".format(BASE_FOLDER)
    OVERLAYS_FOLDER = "{}/overlays".format(BASE_FOLDER)

    template = ""
    overlay = "generic-en"
    language = "en"
