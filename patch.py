import yaml
import jinja2

from config import Config

def patch(template, overlay, output, language, environment):
    """
    1. open overlay
    2. open and patch template
    3. save result in build
    """
    with open(overlay, 'r') as f:
        data = yaml.safe_load(f)
        t = environment.get_template(template)
        with open(output, 'w') as f:
            f.write(t.render(data=data,
                             language=language))


if __name__ == "__main__":
    environment = jinja2.Environment(loader=jinja2.FileSystemLoader(Config.TEMPLATES_FOLDER),
                                     trim_blocks=True,
                                     block_start_string='<%', block_end_string='%>',
                                     variable_start_string='<<', variable_end_string='>>',
                                     comment_start_string='<#', comment_end_string='#>')

    overlay_folder = "{}/{}".format(Config.OVERLAYS_FOLDER, Config.overlay)
    template_folder = Config.TEMPLATES_FOLDER
    output_folder = Config.OUTPUT_FOLDER

    template_overlay = [ (
        "{}.tex".format(x),
        "{}/{}.yml".format(overlay_folder, x),
        "{}/{}.tex".format(output_folder, x)
    ) for x in [
            'personal',
            'education',
            'experience',
            'projects',
            'skills',
            'interests'
        ]
    ]

    for e in template_overlay:
        patch(e[0], e[1], e[2], Config.language, environment)

